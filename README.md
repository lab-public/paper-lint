# Paper Lint

Requirement:

- Git
- Node.js

Usage:

```sh
$ git clone https://gitlab.sis.eecs.tottori-u.ac.jp/lab-public/paper-lint.git
$ cd paper-lint
$ npm install
$ npx textlint paper/*.tex 
```
